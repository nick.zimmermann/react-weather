import React, { useState } from 'react';
import { format } from 'date-fns';
import 'weather-icons/css/weather-icons.css';

const api = {
  key: '8cfb2861b20b5360ac79905848a78480',
  base: 'https://api.openweathermap.org/data/2.5/',
};

const App = () => {
  const [query, setQuery] = useState('');
  const [weather, setWeather] = useState({});
  const [icon, setIcon] = useState('');

  const image = () => {
    if (weather.main.temp > 20) {
      return 'app warm';
    } else if (weather.main.temp < 12) {
      return 'app cold';
    } else {
      return 'app';
    }
  };

  const getIcon = (icon) => fetch(`https://openweathermap.org/img/wn/${icon}@2x.png`).then((res) => setIcon(res));

  const search = (event) => {
    if (event.key === 'Enter') {
      fetch(`${api.base}weather?q=${query}&units=metric&APPID=${api.key}`)
        .then((res) => res.json())
        .then((result) => {
          setWeather(result);
          setQuery('');
          if (result.main) getIcon(result.weather[0].icon);
        });
    }
  };

  const renderWeather = () => {
    if (typeof weather.main !== 'undefined') {
      return (
        <div>
          <div className="location-box">
            <div className="location">
              {weather.name}, {weather.sys.country}
            </div>
            <div className="date">{format(new Date(), 'eeee d MMMM y')}</div>
          </div>
          <div className="weather-box">
            <div className="temp">
              {weather.main.temp.toFixed(1)}°c
              <div className="info">
                Min: {weather.main.temp_min} Max: {weather.main.temp_max}
              </div>
              <div className="feels-like">Feels like: {weather.main.feels_like}</div>
              <div className="weather">
                {weather.weather[0].main}
                <img src={icon.url} alt="icon" />
              </div>
            </div>
          </div>
        </div>
      );
    } else if (weather.cod === '404') {
      return (
        <div className="location-box">
          <div className="location">Nichts gefunden</div>
        </div>
      );
    } else {
      return (
        <div className="location-box">
          <div className="location">Ort eingeben...</div>
        </div>
      );
    }
  };

  return (
    <div className={weather.main ? image() : 'app'}>
      <main>
        <div className="search-box">
          <input type="text" className="search-bar" placeholder="Search..." onChange={(e) => setQuery(e.target.value)} value={query} onKeyPress={search} />
        </div>
        {renderWeather()}
      </main>
    </div>
  );
};

export default App;
